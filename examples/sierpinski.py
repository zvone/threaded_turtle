import turtle
import random

from threaded_turtle import TurtleThread, ThreadSerializer


class Triangle:
    def __init__(self, x, y, size):
        self.x = x
        self.y = y
        self.size = size


class SierpinskiTurtleThread(TurtleThread):
    def __init__(self, ctrl, depth, size, position_x=0, position_y=0):
        super().__init__(ctrl)
        self.depth = depth
        self.size = size
        self.position_x = position_x
        self.position_y = position_y

    def draw_triangle(self, triangle):
        self.turtle.penup()
        self.turtle.goto(triangle.x, triangle.y)
        self.turtle.pendown()
        self.turtle.forward(triangle.size)
        self.turtle.left(120)
        self.turtle.forward(triangle.size)
        self.turtle.left(120)
        self.turtle.forward(triangle.size)
        self.turtle.left(120)

    @staticmethod
    def _get_sub_triangles(triangle):
        x, y, size = triangle.x, triangle.y, triangle.size
        yield Triangle(x, y, size / 2)
        yield Triangle(x + size / 2, y, size / 2)
        yield Triangle(x + size / 4, y + size * 3**0.5 / 4, size / 2)


class BottomUpSierpinskiTurtleThread(SierpinskiTurtleThread):
    def run(self):
        self.turtle.pen(pencolor='#a00000')
        self.turtle.shape('arrow')

        top_triangle = Triangle(self.position_x, self.position_y, self.size)
        for triangle in self._get_triangles(self.depth, top_triangle):
            self.draw_triangle(triangle)

    @classmethod
    def _get_triangles(cls, depth, triangle):
        if depth > 0:
            for sub_triangle in cls._get_sub_triangles(triangle):
                yield from cls._get_triangles(depth-1, sub_triangle)
        else:
            yield triangle


class RandomSierpinskiTurtleThread(BottomUpSierpinskiTurtleThread):
    def run(self):
        self.turtle.pen(pencolor='#0000a0')
        self.turtle.shape('classic')

        top_triangle = Triangle(self.position_x, self.position_y, self.size)
        triangles = list(self._get_triangles(self.depth, top_triangle))
        random.shuffle(triangles)
        for triangle in triangles:
            self.draw_triangle(triangle)


class TopDownSierpinskiTurtleThread(SierpinskiTurtleThread):
    def run(self):
        self.turtle.pen(pencolor='#00a000')
        self.turtle.shape('turtle')

        top_triangle = Triangle(self.position_x, self.position_y, self.size)
        self.draw_triangle(top_triangle)

        triangles = [top_triangle]
        for i in range(self.depth):
            next_triangles = []
            for triangle in triangles:
                self.draw_inner_triangle(triangle)
                next_triangles.extend(self._get_sub_triangles(triangle))
            triangles = next_triangles

    def draw_inner_triangle(self, triangle):
        self.turtle.penup()
        self.turtle.goto(triangle.x + triangle.size / 4,
                  triangle.y + triangle.size * 3**0.5 / 4)
        self.turtle.pendown()
        self.turtle.forward(triangle.size / 2)
        self.turtle.right(120)
        self.turtle.forward(triangle.size / 2)
        self.turtle.right(120)
        self.turtle.forward(triangle.size / 2)
        self.turtle.right(120)


def main():
    depth, size = 4, 200

    window=turtle.Screen()
    window.title('Threaded Sierpinski Turtles')
    window.bgcolor('lightblue')

    ctrl = ThreadSerializer()
    bottom_up_turtle = BottomUpSierpinskiTurtleThread(ctrl, depth, size, -3*size/2, -size * 3**0.5 / 4)
    bottom_up_turtle.start()
    top_down_turtle = TopDownSierpinskiTurtleThread(ctrl, depth, size, -size/2, -size * 3**0.5 / 4)
    top_down_turtle.start()
    random_turtle = RandomSierpinskiTurtleThread(ctrl, depth, size, size/2, -size * 3**0.5 / 4)
    random_turtle.start()
    try:
        ctrl.run_forever()
    except turtle.Terminator:
        print('Terminated.')


if __name__ == "__main__":
    main()

