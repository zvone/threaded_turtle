import queue
from threaded_turtle import ThreadSerializer, TurtleThread


# Initialize one TurtleThreadSerializer
ctrl = ThreadSerializer()

# Initialize TurtleThread instances as you want
# each represents a separate turtle, running in a separate thread

def run(my_turtle, angle):
    my_turtle.left(angle)
    x = 70
    while x > 1:
        my_turtle.left(50)
        my_turtle.forward(x)
        x = 3 * x / 4

turtle1 = TurtleThread(ctrl, target=run, args=(0,))
turtle2 = TurtleThread(ctrl, target=run, args=(45,))
turtle3 = TurtleThread(ctrl, target=run, args=(90,))
turtle4 = TurtleThread(ctrl, target=run, args=(180,))


# Start all turtles:
turtle1.start()
turtle2.start()
turtle3.start()
turtle4.start()

# Start the main controller:
try:
    ctrl.run_forever(queue_timeout = 1)
except queue.Empty:
    print('Done.')

